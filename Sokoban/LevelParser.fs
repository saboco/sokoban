﻿module LevelParser

let rawLevel = """
    ############
    #          #
    # #      $ #
    #       @  #
    # .     $  #
    # .        #
    ############
    """

type Label = string

type TileSymbol = TileSymbol of string

let wallSymbol = TileSymbol "#"
let budySymbol  = TileSymbol "@"
let spaceSymbol  = TileSymbol " "
let slotSymbol  = TileSymbol "."
let boxSymbol  = TileSymbol "$"

type TileLabel = TileLabel of Label
let wallLabel = TileLabel "Wall"
let budyLabel  = TileLabel "Budy"
let spaceLabel  = TileLabel "Space"
let slotLabel  = TileLabel "Slot"
let boxLabel  = TileLabel "Box"

type TileType = 
    | Wall  // #
    | Budy  // @
    | Space // " "
    | Slot  // .
    | Box   // $

type Point = { x : float      
               y : float }

type TilePosition = TilePosition of Point
    
type Tile = 
    { label : TileLabel
      ``type`` : TileType
      position : TilePosition
      symbol: TileSymbol }


let createTile symbol (x, y) =
        match symbol with 
        | '#' -> { label =wallLabel; ``type``= Wall; position  = TilePosition { x=x; y=y}; symbol =  wallSymbol }
        | '@'  -> { label =wallLabel; ``type``= Wall; position  = TilePosition { x=x; y=y}; symbol =  budySymbol }
        | ' ' -> { label =wallLabel; ``type``= Wall; position  = TilePosition { x=x; y=y}; symbol =  spaceSymbol }
        | '.' -> { label =wallLabel; ``type``= Wall; position  = TilePosition { x=x; y=y}; symbol =  slotSymbol }
        | '$' -> { label =wallLabel; ``type``= Wall; position  = TilePosition { x=x; y=y}; symbol =  boxSymbol }
        | c -> failwithf "Symbol was not recognized %c" c


let getPosition x y = x, y


let parseLevel (level:string) =
    let levelArray = List.ofSeq level
    let rec parse x y current tiles =
        match current with
        | '\n'::rest ->  parse 0.0 (y + 1.0) rest tiles
        | c::rest ->  parse (x + 1.0) y rest (tiles @ [createTile c (getPosition x y)])
        | [] -> tiles
    parse 0.0 0.0 levelArray []

let tiles = parseLevel rawLevel

let printTile (t:Tile) =
    let (TilePosition p) = t.position 
    let (TileSymbol symbol) = t.symbol    
    if p.x = 0.0 then
        printfn ""
        printf "%s" symbol
    else         
        printf "%s" symbol    

let printTiles (tiles: Tile list) = 
    Seq.iter printTile tiles
    printfn ""

printTiles tiles

